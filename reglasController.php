<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\reglas;

class cliente extends Controller
{

    public function index(){
        return \App\reglas::all();//
    }

    //get,select
    public function show($id){
        return \App\reglas::find($id);//
    }

    //post,insert
    public function store(Request $request){
        return \App\reglas::create($request->all());//los names de los inputs tienen que ser igual, pasarse todos los campos
    }
    //put,update
    public function update(Request $request,$id){

        $registro=\App\reglas::findOrFail($id);
        $registro-> update($request->all());
        return $registro;
    }
    //delete
    public function destroy($id){
        $registro=\App\reglas::findOrFail($id);// http crea todo un set de codigos
        $registro-> delete();
        return 204;//muestra una ejecucion existosa

    }
}
