<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutentificacionMD extends Model
{
    public $timestamps=false;
    protected $table ="autentificaciones";//el nombre de la tabla de la base de datos
    protected  $fillable = array('Codigo','Autentificacion');
}
