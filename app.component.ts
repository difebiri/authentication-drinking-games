import { Component } from '@angular/core';
import {AuthService,SocialUser,GoogleLoginProvider,FacebookLoginProvider} from 'ng4-social-login';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'proyecto-final';
  public user: any = SocialUser;
  constructor(private socialAuthSerice:AuthService){}
  facebooklogin(){
    this.socialAuthSerice.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData) => {
      this.user=userData;
    });
  }
  
  googlelogin(){
    this.socialAuthSerice.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData)=>{
      this.user=userData;
    });
  }
}
